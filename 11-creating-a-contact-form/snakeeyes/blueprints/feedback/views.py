from flask import (
    Blueprint,
    flash,
    redirect,
    request,
    url_for,
    render_template)

from snakeeyes.blueprints.feedback.forms import FeedbackForm

feedback = Blueprint('feedback', __name__, template_folder='templates')

# Rendering the from is a 'GET' request
# submissionn of Form with data is a 'POST' request
@feedback.route('/feedback', methods=['GET', 'POST'])
def index():
    form = FeedbackForm()

    if form.validate_on_submit():
        # This prevents circular imports.
        from snakeeyes.blueprints.feedback.tasks import deliver_feedback_email

        deliver_feedback_email.delay(request.form.get('email'),
                                    request.form.get('message'))

        flash('Thanks, expect a response shortly.', 'success')
        return redirect(url_for('feedback.index'))
        
# sends form back with origianl data if there is an error
    return render_template('feedback/index.html', form=form)
