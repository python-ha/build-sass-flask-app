from flask_wtf import Form
from wtforms import TextAreaField
from wtforms_components import EmailField
from wtforms.validators import DataRequired, Length

class FeedbackForm(Form):
    email = EmailField("What's your e-mail address?",
                       [DataRequired(), Length(3, 254)])
    message = TextAreaField("Is there any feedback you would like to give us?",
                            [DataRequired(), Length(1, 8192)])
